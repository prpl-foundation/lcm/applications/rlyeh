/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <setjmp.h>
#include <cmocka.h>
#include <unistd.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_object_expression.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>

#include <rlyeh/rlyeh.h>

#include "test_datamodel.h"
#include "../../include_priv/rlyeh.h"
#include "../../include_priv/rlyeh_dm.h"
#include "../../include_priv/rlyeh_worker_signals.h"

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>

#include <amxc/amxc_macros.h>
#include <amxut/amxut_macros.h>

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;

static void handle_events(void) {
    amxut_bus_handle_events();
    worker_notification_handler(worker_sig_ctrl_fd(), NULL);
}

static int test_rlyeh_gc(void) {
    int ret;
    amxc_var_t* ret_args = NULL;
    amxc_var_t* rlyeh_args = NULL;
    amxc_var_new(&rlyeh_args);
    amxc_var_new(&ret_args);

    amxc_var_set_type(rlyeh_args, AMXC_VAR_ID_HTABLE);
    ret = amxb_call(amxut_bus_ctx(), RLYEH_DM, "gc", rlyeh_args, ret_args, 5);
    handle_events();

    amxc_var_delete(&rlyeh_args);
    amxc_var_delete(&ret_args);
    return ret;
}

static int test_rlyeh_sv(bool enable) {
    int ret;
    amxc_var_t* ret_args = NULL;
    amxc_var_t* rlyeh_args = NULL;
    amxc_var_new(&rlyeh_args);
    amxc_var_new(&ret_args);

    amxc_var_set_type(rlyeh_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, rlyeh_args, RLYEH_CMD_SV_ENABLE, enable);

    ret = amxb_call(amxut_bus_ctx(), RLYEH_DM, "sv", rlyeh_args, ret_args, 5);
    handle_events();

    amxc_var_delete(&rlyeh_args);
    amxc_var_delete(&ret_args);
    return ret;
}


static int test_rlyeh_status(const char* duid, const char* version) {
    int ret;
    amxc_var_t* ret_args = NULL;
    amxc_var_t* rlyeh_args = NULL;
    amxc_var_new(&rlyeh_args);
    amxc_var_new(&ret_args);

    amxc_var_set_type(rlyeh_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_CMD_PULL_DUID, duid);
    amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_DM_IMAGE_VERSION, version);

    ret = amxb_call(amxut_bus_ctx(), RLYEH_DM, "status", rlyeh_args, ret_args, 5);
    handle_events();

    amxc_var_delete(&rlyeh_args);
    amxc_var_delete(&ret_args);
    return ret;
}

static int test_rlyeh_remove(const char* duid, const char* version) {
    int ret;
    amxc_var_t* ret_args = NULL;
    amxc_var_t* rlyeh_args = NULL;
    amxc_var_new(&rlyeh_args);
    amxc_var_new(&ret_args);

    amxc_var_set_type(rlyeh_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_CMD_PULL_DUID, duid);
    amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_DM_IMAGE_VERSION, version);

    ret = amxb_call(amxut_bus_ctx(), RLYEH_DM, "remove", rlyeh_args, ret_args, 5);
    handle_events();

    amxc_var_delete(&rlyeh_args);
    amxc_var_delete(&ret_args);
    return ret;
}

static int test_rlyeh_list(void) {
    int ret;
    amxc_var_t* ret_args = NULL;
    amxc_var_t* rlyeh_args = NULL;
    amxc_var_new(&rlyeh_args);
    amxc_var_new(&ret_args);

    amxc_var_set_type(rlyeh_args, AMXC_VAR_ID_HTABLE);
    ret = amxb_call(amxut_bus_ctx(), RLYEH_DM, "list", rlyeh_args, ret_args, 5);
    handle_events();

    amxc_var_delete(&rlyeh_args);
    amxc_var_delete(&ret_args);
    return ret;
}

static int test_rlyeh_pull(const char* uri, const char* duid, const char* username, const char* password) {
    int ret;
    amxc_var_t* ret_args = NULL;
    amxc_var_t* rlyeh_args = NULL;
    amxc_var_new(&rlyeh_args);
    amxc_var_new(&ret_args);

    amxc_var_set_type(rlyeh_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_CMD_PULL_URI, uri);
    amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_CMD_PULL_DUID, duid);
    if(username) {
        amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_CMD_PULL_USERNAME, username);
    }
    if(password) {
        amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_CMD_PULL_PASSWORD, password);
    }

    ret = amxb_call(amxut_bus_ctx(), RLYEH_DM, "pull", rlyeh_args, ret_args, 5);
    handle_events();

    amxc_var_delete(&rlyeh_args);
    amxc_var_delete(&ret_args);
    return ret;
}

int test_dm_setup(void** state) {
    const char* definition_file = "../../odl/rlyeh_definition.odl";
    const char* defaults_file = "./rlyeh_defaults.test.odl";

    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_copy");
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, "rlyeh_images");
    sahTraceAddZone(500, "rlyeh_remove");
    sahTraceAddZone(500, "rlyeh_curl");

    sahTraceAddZone(500, "rlyeh_events");
    sahTraceAddZone(500, "rlyeh_notif");
    sahTraceAddZone(500, "rlyeh_signature");
    sahTraceAddZone(500, "rlyeh_functions");

    sahTraceAddZone(500, __FILE__);
    sahTraceOpen(__FILE__, TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO(__FILE__, "sahTrace initialized");

    amxut_bus_setup(state);

    amxut_resolve_function("pull", _Rlyeh_pull);
    amxut_resolve_function("remove", _Rlyeh_remove);
    amxut_resolve_function("gc", _Rlyeh_gc);
    amxut_resolve_function("sv", _Rlyeh_sv);
    amxut_resolve_function("status", _Rlyeh_status);
    amxut_resolve_function("list", _Rlyeh_list);
    amxut_resolve_function("command", _Rlyeh_command);

    amxut_dm_load_odl(definition_file);
    amxut_dm_load_odl(defaults_file);

    dm = amxut_bus_dm();
    parser = amxut_bus_parser();

    rlyeh_remove_dir_unsafe("/tmp/rlyeh_test_datamodel");
    rlyeh_mkdir("/tmp/rlyeh_test_datamodel", false);
    rlyeh_mkdir("/tmp/rlyeh_test_datamodel/images", false);
    rlyeh_mkdir("/tmp/rlyeh_test_datamodel/blobs", false);
    rlyeh_mkdir("/tmp/rlyeh_test_datamodel/ro_blobs", false);
    rlyeh_mkdir("/tmp/rlyeh_test_datamodel/ro_images", false);
    return 0;
}

int test_dm_teardown(void** state) {
    amxut_bus_teardown(state);
    rlyeh_remove_dir_unsafe("/tmp/rlyeh_test_datamodel");
    sahTraceClose();
    return 0;
}

void test_dm_init_no_images(UNUSED void** state) {
    int retval;

    sahTraceAddZone(500, __func__);
    SAH_TRACEZ_INFO(__func__, "Start rlyeh");
    _rlyeh_main(0, dm, parser);
    handle_events();

    retval = test_rlyeh_status("dad7ed90-ee42-5e86-a229-618ba170d4a4", "latest");
    assert_int_equal(retval, amxd_status_object_not_found);

    retval = test_rlyeh_list();
    assert_int_equal(retval, amxd_status_ok);
    handle_events();

    retval = test_rlyeh_gc();
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(10);
    handle_events();

    SAH_TRACEZ_INFO(__func__, "Stop rlyeh");
    _rlyeh_main(1, dm, parser);
}

void test_dm_init_with_images(UNUSED void** state) {
    rlyeh_status_t status = RLYEH_NO_ERROR;
    rlyeh_copy_data_t data;
    char err_msg[RLYEH_ERR_MSG_LEN] = "";
    amxc_var_t args;
    int retval;

    sahTraceAddZone(500, __func__);

    SAH_TRACEZ_INFO(__func__, "Init curl");
    rlyeh_curl_init();

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://registry-1.docker.io/lcmtestcontainers/alpine:latest");
    data.destination = strdup("oci:///tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine:latest");
    data.dest_shared_blob_dir = strdup("/tmp/rlyeh_test_datamodel/blobs");
    data.images_location = strdup("/tmp/rlyeh_test_datamodel/images");
    data.duid = strdup("dad7ed90-ee42-5e86-a229-618ba170d4a4");

    SAH_TRACEZ_INFO(__func__, "Getting image #1");

    status = rlyeh_copy(&data, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/oci-layout"));
    rlyeh_copy_data_clean(&data);

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://registry-1.docker.io/lcmtestcontainers/alpine_annotated_test:3.19");
    data.destination = strdup("oci:///tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated:3.19");
    data.dest_shared_blob_dir = strdup("/tmp/rlyeh_test_datamodel/blobs");
    data.images_location = strdup("/tmp/rlyeh_test_datamodel/images");
    data.duid = strdup("6f3b69f0-482d-5eab-9df8-b9e856f6cb38");

    SAH_TRACEZ_INFO(__func__, "Getting image #2");

    status = rlyeh_copy(&data, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/oci-layout"));
    rlyeh_copy_data_clean(&data);

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://registry-1.docker.io/lcmtestcontainers/alpine_annotated_test:3.18");
    data.destination = strdup("oci:///tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated:3.18");
    data.dest_shared_blob_dir = strdup("/tmp/rlyeh_test_datamodel/blobs");
    data.images_location = strdup("/tmp/rlyeh_test_datamodel/images");
    data.duid = strdup("6f3b69f0-482d-5eab-9df8-b9e856f6cb38");

    SAH_TRACEZ_INFO(__func__, "Getting image #3");

    status = rlyeh_copy(&data, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated/oci-layout"));
    retval = rlyeh_update_index_annotations("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated", "6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.18", RLYEH_ANNOTATION_LCM_MAKRFORREMOVAL, "1");
    assert_true(retval > 0);
    rlyeh_copy_data_clean(&data);

    // TODO configure config
    SAH_TRACEZ_INFO(__func__, "Starting Rlyeh");
    _rlyeh_main(0, dm, parser);
    handle_events();

    amxc_var_init(&args);
    assert_int_equal(amxb_get(amxut_bus_ctx(), RLYEH_DM_IMAGES ".", INT32_MAX, &args, 5), 0);
    amxc_var_dump(&args, STDOUT_FILENO);
    amxc_var_clean(&args);

    SAH_TRACEZ_INFO(__func__, "Do checks to test datamodel");
    amxd_object_t* images = amxd_dm_findf(dm, RLYEH_DM_IMAGES);
    assert_non_null(images);
    uint32_t images_count = amxd_object_get_instance_count(images);
    assert_int_equal(images_count, 3);

    amxd_object_t* image_alpine_latest = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", "dad7ed90-ee42-5e86-a229-618ba170d4a4", "latest");
    assert_non_null(image_alpine_latest);

    amxc_var_init(&args);

    amxd_object_get_params(image_alpine_latest, &args, amxd_dm_access_protected);
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_DESCRIPTION), "");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VENDOR), "");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_NAME), "lcmtestcontainers/alpine");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VERSION), "latest");
    assert_non_null(GET_ARG(&args, RLYEH_DM_IMAGE_DISKLOCATION));
    assert_false(GET_BOOL(&args, RLYEH_DM_IMAGE_MARK_RM));

    amxc_var_clean(&args);

    retval = test_rlyeh_status("dad7ed90-ee42-5e86-a229-618ba170d4a4", "latest");
    assert_int_equal(retval, amxd_status_ok);

    amxd_object_t* image_alpine_annotated_318 = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", "6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.18");
    assert_non_null(image_alpine_annotated_318);

    amxc_var_init(&args);

    amxd_object_get_params(image_alpine_annotated_318, &args, amxd_dm_access_protected);
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_DESCRIPTION), "Annotated version of alpine 3.18");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VENDOR), "lcmtestcontainers");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_NAME), "alpine_annotated");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VERSION), "3.18");
    assert_non_null(GET_ARG(&args, RLYEH_DM_IMAGE_DISKLOCATION));
    assert_true(GET_BOOL(&args, RLYEH_DM_IMAGE_MARK_RM));

    amxc_var_clean(&args);

    retval = test_rlyeh_status("6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.18");
    assert_int_equal(retval, amxd_status_ok);

    amxd_object_t* image_alpine_annotated_319 = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", "6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.19");
    assert_non_null(image_alpine_annotated_319);

    amxc_var_init(&args);

    amxd_object_get_params(image_alpine_annotated_319, &args, amxd_dm_access_protected);
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_DESCRIPTION), "Annotated version of alpine 3.19");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VENDOR), "lcmtestcontainers");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_NAME), "alpine_annotated");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VERSION), "3.19");
    assert_non_null(GET_ARG(&args, RLYEH_DM_IMAGE_DISKLOCATION));
    assert_false(GET_BOOL(&args, RLYEH_DM_IMAGE_MARK_RM));

    amxc_var_clean(&args);

    retval = test_rlyeh_status("6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.19");
    assert_int_equal(retval, amxd_status_ok);

    retval = test_rlyeh_list();
    assert_int_equal(retval, amxd_status_ok);
    handle_events();

    retval = test_rlyeh_status("6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.18");
    assert_int_equal(retval, amxd_status_ok);
    handle_events();

    retval = test_rlyeh_gc();
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(10);
    handle_events();

    SAH_TRACEZ_INFO(__func__, "Stop rlyeh");
    _rlyeh_main(1, dm, parser);
}

void test_dm_init_ro_images(UNUSED void** state) {
    rlyeh_status_t status = RLYEH_NO_ERROR;
    rlyeh_copy_data_t data;
    char err_msg[RLYEH_ERR_MSG_LEN] = "";
    amxc_var_t args;
    int retval;

    sahTraceAddZone(500, __func__);

    SAH_TRACEZ_INFO(__func__, "Init curl");
    rlyeh_curl_init();

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://registry-1.docker.io/lcmtestcontainers/alpine:latest");
    data.destination = strdup("oci:///tmp/rlyeh_test_datamodel/ro_images/lcmtestcontainers/alpine:latest");
    data.dest_shared_blob_dir = strdup("/tmp/rlyeh_test_datamodel/ro_blobs");
    data.images_location = strdup("/tmp/rlyeh_test_datamodel/ro_images");
    data.duid = strdup("dad7ed90-ee42-5e86-a229-618ba170d4a4");

    SAH_TRACEZ_INFO(__func__, "Getting image #1");

    status = rlyeh_copy(&data, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/ro_images/lcmtestcontainers/alpine/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/ro_images/lcmtestcontainers/alpine/oci-layout"));
    rlyeh_copy_data_clean(&data);

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://registry-1.docker.io/lcmtestcontainers/alpine_annotated_test:3.18");
    data.destination = strdup("oci:///tmp/rlyeh_test_datamodel/ro_images/lcmtestcontainers/alpine_annotated:3.18");
    data.dest_shared_blob_dir = strdup("/tmp/rlyeh_test_datamodel/ro_blobs");
    data.images_location = strdup("/tmp/rlyeh_test_datamodel/ro_images");
    data.duid = strdup("6f3b69f0-482d-5eab-9df8-b9e856f6cb38");

    SAH_TRACEZ_INFO(__func__, "Getting image #2");

    status = rlyeh_copy(&data, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/ro_images/lcmtestcontainers/alpine_annotated/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/ro_images/lcmtestcontainers/alpine_annotated/oci-layout"));
    rlyeh_copy_data_clean(&data);

    SAH_TRACEZ_INFO(__func__, "Start rlyeh");
    _rlyeh_main(0, dm, parser);
    handle_events();

    amxc_var_init(&args);
    assert_int_equal(amxb_get(amxut_bus_ctx(), RLYEH_DM_IMAGES ".", INT32_MAX, &args, 5), 0);
    amxc_var_dump(&args, STDOUT_FILENO);
    amxc_var_clean(&args);

    SAH_TRACEZ_INFO(__func__, "Do checks to test datamodel");
    amxd_object_t* images = amxd_dm_findf(dm, RLYEH_DM_IMAGES);
    assert_non_null(images);
    uint32_t images_count = amxd_object_get_instance_count(images);
    assert_int_equal(images_count, 2);

    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/oci-layout"));

    amxd_object_t* image_alpine_latest = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", "dad7ed90-ee42-5e86-a229-618ba170d4a4", "latest");
    assert_non_null(image_alpine_latest);

    amxc_var_init(&args);

    amxd_object_get_params(image_alpine_latest, &args, amxd_dm_access_protected);
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_DESCRIPTION), "");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VENDOR), "");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_NAME), "lcmtestcontainers/alpine");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VERSION), "latest");
    assert_non_null(GET_ARG(&args, RLYEH_DM_IMAGE_DISKLOCATION));
    assert_false(GET_BOOL(&args, RLYEH_DM_IMAGE_MARK_RM));

    amxc_var_clean(&args);

    retval = test_rlyeh_status("dad7ed90-ee42-5e86-a229-618ba170d4a4", "latest");
    assert_int_equal(retval, amxd_status_ok);

    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated/oci-layout"));

    amxd_object_t* image_alpine_annotated_318 = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", "6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.18");
    assert_non_null(image_alpine_annotated_318);

    amxc_var_init(&args);

    amxd_object_get_params(image_alpine_annotated_318, &args, amxd_dm_access_protected);
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_DESCRIPTION), "Annotated version of alpine 3.18");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VENDOR), "lcmtestcontainers");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_NAME), "alpine_annotated");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VERSION), "3.18");
    assert_non_null(GET_ARG(&args, RLYEH_DM_IMAGE_DISKLOCATION));
    assert_false(GET_BOOL(&args, RLYEH_DM_IMAGE_MARK_RM));

    amxc_var_clean(&args);

    retval = test_rlyeh_status("6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.18");
    assert_int_equal(retval, amxd_status_ok);

    SAH_TRACEZ_INFO(__func__, "Do checks to see if the blobs are populated");
    assert_false(rlyeh_is_empty_dir("/tmp/rlyeh_test_datamodel/blobs"));
    assert_false(rlyeh_is_empty_dir("/tmp/rlyeh_test_datamodel/blobs/sha256/"));

    test_rlyeh_list();
    handle_events();

    test_rlyeh_gc();
    handle_events();
    sleep(10);
    handle_events();

    SAH_TRACEZ_INFO(__func__, "Stop rlyeh");
    _rlyeh_main(1, dm, parser);
}

void test_dm_pull_images(UNUSED void** state) {
    amxd_status_t retval;
    amxc_var_t args;
    sahTraceAddZone(500, __func__);

    SAH_TRACEZ_INFO(__func__, "Start rlyeh");
    _rlyeh_main(0, dm, parser);
    handle_events();

    SAH_TRACEZ_INFO(__func__, "Pull failed image #1");
    retval = test_rlyeh_pull("docker://unrealserver.com/shouldntexist/shouldntexistimage:latest", "5aa7ed90-e242-5e26-a229-338ba170d4a4", NULL, NULL);
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(180);
    handle_events();

    retval = test_rlyeh_status("5aa7ed90-e242-5e26-a229-338ba170d4a4", "latest");
    assert_int_equal(retval, amxd_status_object_not_found);

    assert_false(file_exists("/tmp/rlyeh_test_datamodel/images/shouldntexist/shouldntexistimage/index.json"));
    assert_false(file_exists("/tmp/rlyeh_test_datamodel/images/shouldntexist/shouldntexistimage/oci-layout"));

    SAH_TRACEZ_INFO(__func__, "Pull failed image #2");
    retval = test_rlyeh_pull("docker://registry-1.docker.io/library/blablaname:blablaversion", "5aa7ed90-e242-5e26-a229-338ba170d4a4", NULL, NULL);
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(20);
    handle_events();

    retval = test_rlyeh_status("5aa7ed90-e242-5e26-a229-338ba170d4a4", "blablaversion");
    assert_int_equal(retval, amxd_status_object_not_found);

    assert_false(file_exists("/tmp/rlyeh_test_datamodel/images/shouldntexist/blablaname/index.json"));
    assert_false(file_exists("/tmp/rlyeh_test_datamodel/images/shouldntexist/blablaname/oci-layout"));

    SAH_TRACEZ_INFO(__func__, "Pull failed image #3");
    retval = test_rlyeh_pull("", "5aa7ed90-e242-5e26-a229-338ba170d4a4", NULL, NULL);
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(10);
    handle_events();

    SAH_TRACEZ_INFO(__func__, "Pull failed image #4");
    retval = test_rlyeh_pull("docker://registry-1.docker.io/lcmtestcontainers/alpine:latest", "", NULL, NULL);
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(10);
    handle_events();

    assert_false(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/index.json"));
    assert_false(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/oci-layout"));

    SAH_TRACEZ_INFO(__func__, "Pull image #1");
    retval = test_rlyeh_pull("docker://registry-1.docker.io/lcmtestcontainers/alpine:latest", "dad7ed90-ee42-5e86-a229-618ba170d4a4", NULL, NULL);
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(60);
    handle_events();

    retval = test_rlyeh_status("dad7ed90-ee42-5e86-a229-618ba170d4a4", "latest");
    assert_int_equal(retval, amxd_status_ok);

    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/oci-layout"));

    SAH_TRACEZ_INFO(__func__, "Pull image #2");
    retval = test_rlyeh_pull("docker://registry-1.docker.io/lcmtestcontainers/alpine_annotated_test:3.18", "6f3b69f0-482d-5eab-9df8-b9e856f6cb38", NULL, NULL);
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(60);
    handle_events();

    retval = test_rlyeh_status("6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.18");
    assert_int_equal(retval, amxd_status_ok);

    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated_test/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated_test/oci-layout"));

    SAH_TRACEZ_INFO(__func__, "Pull image #3");
    retval = test_rlyeh_pull("docker://registry-1.docker.io/lcmtestcontainers/alpine_annotated_test:3.19", "6f3b69f0-482d-5eab-9df8-b9e856f6cb38", NULL, NULL);
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(60);
    handle_events();

    retval = test_rlyeh_status("6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.19");
    assert_int_equal(retval, amxd_status_ok);

    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated_test/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated_test/oci-layout"));

    handle_events();

    SAH_TRACEZ_INFO(__func__, "Pull failed image #5");
    retval = test_rlyeh_pull("docker://registry-1.docker.io/lcmtestcontainers/ubuntu_private_modified_annotated:priv_18.04", "873b69f0-482d-3aaa-9df8-b9e856f6cb38", "wrong", "gnorw");
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(20);
    handle_events();

    retval = test_rlyeh_status("873b69f0-482d-3aaa-9df8-b9e856f6cb38", "priv_18.04");
    assert_int_equal(retval, amxd_status_object_not_found);

    assert_false(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/ubuntu_private_modified_annotated/index.json"));
    assert_false(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/ubuntu_private_modified_annotated/oci-layout"));

    handle_events();

    SAH_TRACEZ_INFO(__func__, "Pull image #4");
    retval = test_rlyeh_pull("docker://registry-1.docker.io/lcmtestcontainers/ubuntu_private_modified_annotated:priv_18.04", "873b69f0-482d-3aaa-9df8-b9e856f6cb38", "lcmtestcontainers", "A6Kdw2UyiLnYmzI6b9kO");
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(60);
    handle_events();

    retval = test_rlyeh_status("873b69f0-482d-3aaa-9df8-b9e856f6cb38", "priv_18.04");
    assert_int_equal(retval, amxd_status_ok);

    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/ubuntu_private_modified_annotated/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/ubuntu_private_modified_annotated/oci-layout"));

    handle_events();

    SAH_TRACEZ_INFO(__func__, "Pull image #5");
    retval = test_rlyeh_pull("docker://registry-1.docker.io/lcmtestcontainers/ubuntu_private_modified_annotated:priv_20.04", "873b69f0-482d-3aaa-9df8-b9e856f6cb38", "lcmtestcontainers", "A6Kdw2UyiLnYmzI6b9kO");
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(60);
    handle_events();

    retval = test_rlyeh_status("873b69f0-482d-3aaa-9df8-b9e856f6cb38", "priv_20.04");
    assert_int_equal(retval, amxd_status_ok);

    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/ubuntu_private_modified_annotated/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/ubuntu_private_modified_annotated/oci-layout"));

    handle_events();

    SAH_TRACEZ_INFO(__func__, "Do checks to test datamodel");

    amxc_var_init(&args);
    assert_int_equal(amxb_get(amxut_bus_ctx(), RLYEH_DM_IMAGES ".", INT32_MAX, &args, 5), 0);
    amxc_var_dump(&args, STDOUT_FILENO);

    amxc_var_clean(&args);

    amxd_object_t* images = amxd_dm_findf(dm, RLYEH_DM_IMAGES);
    assert_non_null(images);
    uint32_t images_count = amxd_object_get_instance_count(images);
    assert_int_equal(images_count, 5);

    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/oci-layout"));

    amxd_object_t* image_alpine_latest = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", "dad7ed90-ee42-5e86-a229-618ba170d4a4", "latest");
    assert_non_null(image_alpine_latest);

    amxc_var_init(&args);

    amxd_object_get_params(image_alpine_latest, &args, amxd_dm_access_protected);
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_DESCRIPTION), "");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VENDOR), "");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_NAME), "lcmtestcontainers/alpine");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VERSION), "latest");
    assert_false(GET_BOOL(&args, RLYEH_DM_IMAGE_MARK_RM));
    assert_non_null(GET_ARG(&args, RLYEH_DM_IMAGE_DISKLOCATION));

    amxc_var_clean(&args);

    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated_test/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated_test/oci-layout"));

    amxd_object_t* image_alpine_annotated_318 = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", "6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.18");
    assert_non_null(image_alpine_annotated_318);

    amxc_var_init(&args);

    amxd_object_get_params(image_alpine_annotated_318, &args, amxd_dm_access_protected);
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_DESCRIPTION), "Annotated version of alpine 3.18");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VENDOR), "lcmtestcontainers");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_NAME), "alpine_annotated");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VERSION), "3.18");
    assert_false(GET_BOOL(&args, RLYEH_DM_IMAGE_MARK_RM));
    assert_non_null(GET_ARG(&args, RLYEH_DM_IMAGE_DISKLOCATION));

    amxc_var_clean(&args);

    amxd_object_t* image_alpine_annotated_319 = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", "6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.19");
    assert_non_null(image_alpine_annotated_319);

    amxc_var_init(&args);

    amxd_object_get_params(image_alpine_annotated_319, &args, amxd_dm_access_protected);
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_DESCRIPTION), "Annotated version of alpine 3.19");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VENDOR), "lcmtestcontainers");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_NAME), "alpine_annotated");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VERSION), "3.19");
    assert_false(GET_BOOL(&args, RLYEH_DM_IMAGE_MARK_RM));
    assert_non_null(GET_ARG(&args, RLYEH_DM_IMAGE_DISKLOCATION));

    amxc_var_clean(&args);

    amxd_object_t* ubuntu_private_modified_annotated_1804 = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", "873b69f0-482d-3aaa-9df8-b9e856f6cb38", "priv_18.04");
    assert_non_null(ubuntu_private_modified_annotated_1804);

    amxc_var_init(&args);

    amxd_object_get_params(ubuntu_private_modified_annotated_1804, &args, amxd_dm_access_protected);
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VENDOR), "lcmtestcontainers");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_NAME), "ubuntu_private_modified_annotated");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VERSION), "priv_18.04");
    assert_false(GET_BOOL(&args, RLYEH_DM_IMAGE_MARK_RM));
    assert_non_null(GET_ARG(&args, RLYEH_DM_IMAGE_DISKLOCATION));

    amxc_var_clean(&args);

    amxd_object_t* ubuntu_private_modified_annotated_2004 = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", "873b69f0-482d-3aaa-9df8-b9e856f6cb38", "priv_20.04");
    assert_non_null(ubuntu_private_modified_annotated_2004);

    amxc_var_init(&args);

    amxd_object_get_params(ubuntu_private_modified_annotated_2004, &args, amxd_dm_access_protected);
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VENDOR), "lcmtestcontainers");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_NAME), "ubuntu_private_modified_annotated");
    assert_string_equal(GET_CHAR(&args, RLYEH_DM_IMAGE_VERSION), "priv_20.04");
    assert_false(GET_BOOL(&args, RLYEH_DM_IMAGE_MARK_RM));
    assert_non_null(GET_ARG(&args, RLYEH_DM_IMAGE_DISKLOCATION));

    amxc_var_clean(&args);

    test_rlyeh_list();
    handle_events();

    retval = test_rlyeh_status("dad7ed90-ee42-5e86-a229-618ba170d4a4", "latest");
    assert_int_equal(retval, amxd_status_ok);

    retval = test_rlyeh_status("6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.18");
    assert_int_equal(retval, amxd_status_ok);

    retval = test_rlyeh_status("6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.19");
    assert_int_equal(retval, amxd_status_ok);

    retval = test_rlyeh_status("873b69f0-482d-3aaa-9df8-b9e856f6cb38", "priv_18.04");
    assert_int_equal(retval, amxd_status_ok);

    retval = test_rlyeh_status("873b69f0-482d-3aaa-9df8-b9e856f6cb38", "priv_20.04");
    assert_int_equal(retval, amxd_status_ok);

    retval = test_rlyeh_gc();
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(20);
    handle_events();

    SAH_TRACEZ_INFO(__func__, "Do checks to see if the blobs are populated");
    assert_false(rlyeh_is_empty_dir("/tmp/rlyeh_test_datamodel/blobs"));
    assert_false(rlyeh_is_empty_dir("/tmp/rlyeh_test_datamodel/blobs/sha256/"));

    SAH_TRACEZ_INFO(__func__, "Stop rlyeh");
    _rlyeh_main(1, dm, parser);
}


void test_dm_remove_images(UNUSED void** state) {
    rlyeh_status_t status = RLYEH_NO_ERROR;
    rlyeh_copy_data_t data;
    char err_msg[RLYEH_ERR_MSG_LEN] = "";
    amxc_var_t args;
    int retval;
    amxd_object_t* images;
    uint32_t images_count;

    sahTraceAddZone(500, __func__);

    SAH_TRACEZ_INFO(__func__, "Init curl");
    rlyeh_curl_init();

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://registry-1.docker.io/lcmtestcontainers/alpine:latest");
    data.destination = strdup("oci:///tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine:latest");
    data.dest_shared_blob_dir = strdup("/tmp/rlyeh_test_datamodel/blobs");
    data.images_location = strdup("/tmp/rlyeh_test_datamodel/images");
    data.duid = strdup("dad7ed90-ee42-5e86-a229-618ba170d4a4");

    SAH_TRACEZ_INFO(__func__, "Getting image #1");

    status = rlyeh_copy(&data, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/oci-layout"));
    rlyeh_copy_data_clean(&data);

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://registry-1.docker.io/lcmtestcontainers/alpine_annotated_test:3.19");
    data.destination = strdup("oci:///tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated:3.19");
    data.dest_shared_blob_dir = strdup("/tmp/rlyeh_test_datamodel/blobs");
    data.images_location = strdup("/tmp/rlyeh_test_datamodel/images");
    data.duid = strdup("6f3b69f0-482d-5eab-9df8-b9e856f6cb38");

    SAH_TRACEZ_INFO(__func__, "Getting image #2");

    status = rlyeh_copy(&data, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/oci-layout"));
    rlyeh_copy_data_clean(&data);

    rlyeh_copy_data_init(&data);
    data.source = strdup("docker://registry-1.docker.io/lcmtestcontainers/alpine_annotated_test:3.18");
    data.destination = strdup("oci:///tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated:3.18");
    data.dest_shared_blob_dir = strdup("/tmp/rlyeh_test_datamodel/blobs");
    data.images_location = strdup("/tmp/rlyeh_test_datamodel/images");
    data.duid = strdup("6f3b69f0-482d-5eab-9df8-b9e856f6cb38");

    SAH_TRACEZ_INFO(__func__, "Getting image #3");

    status = rlyeh_copy(&data, err_msg);
    assert_int_equal(status, RLYEH_NO_ERROR);
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated/oci-layout"));
    rlyeh_copy_data_clean(&data);

    // TODO configure config
    SAH_TRACEZ_INFO(__func__, "Starting Rlyeh");
    _rlyeh_main(0, dm, parser);
    handle_events();

    amxc_var_init(&args);
    assert_int_equal(amxb_get(amxut_bus_ctx(), RLYEH_DM_IMAGES ".", INT32_MAX, &args, 5), 0);
    amxc_var_dump(&args, STDOUT_FILENO);
    amxc_var_clean(&args);

    handle_events();

    images = amxd_dm_findf(dm, RLYEH_DM_IMAGES);
    assert_non_null(images);
    images_count = amxd_object_get_instance_count(images);
    assert_int_equal(images_count, 3);

    SAH_TRACEZ_INFO(__func__, "Remove with Empty version");
    retval = test_rlyeh_remove("brokenuuid", "");
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(10);
    handle_events();

    amxc_var_init(&args);
    assert_int_equal(amxb_get(amxut_bus_ctx(), RLYEH_DM_IMAGES ".", INT32_MAX, &args, 5), 0);
    amxc_var_dump(&args, STDOUT_FILENO);
    amxc_var_clean(&args);

    images = amxd_dm_findf(dm, RLYEH_DM_IMAGES);
    assert_non_null(images);
    images_count = amxd_object_get_instance_count(images);
    assert_int_equal(images_count, 3);

    SAH_TRACEZ_INFO(__func__, "Remove with Empty uuid");
    retval = test_rlyeh_remove("", "brokenversion");
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(10);
    handle_events();

    amxc_var_init(&args);
    assert_int_equal(amxb_get(amxut_bus_ctx(), RLYEH_DM_IMAGES ".", INT32_MAX, &args, 5), 0);
    amxc_var_dump(&args, STDOUT_FILENO);
    amxc_var_clean(&args);

    images = amxd_dm_findf(dm, RLYEH_DM_IMAGES);
    assert_non_null(images);
    images_count = amxd_object_get_instance_count(images);
    assert_int_equal(images_count, 3);

    SAH_TRACEZ_INFO(__func__, "Remove brokenuuid and brokenversion");
    retval = test_rlyeh_remove("brokenuuid", "brokenversion");
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(10);
    handle_events();

    amxc_var_init(&args);
    assert_int_equal(amxb_get(amxut_bus_ctx(), RLYEH_DM_IMAGES ".", INT32_MAX, &args, 5), 0);
    amxc_var_dump(&args, STDOUT_FILENO);
    amxc_var_clean(&args);

    images = amxd_dm_findf(dm, RLYEH_DM_IMAGES);
    assert_non_null(images);
    images_count = amxd_object_get_instance_count(images);
    assert_int_equal(images_count, 3);

    SAH_TRACEZ_INFO(__func__, "Remove 6f3b69f0-482d-5eab-9df8-b9e856f6cb38:3.18");
    retval = test_rlyeh_remove("6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.18");
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(10);
    handle_events();

    amxc_var_init(&args);
    assert_int_equal(amxb_get(amxut_bus_ctx(), RLYEH_DM_IMAGES ".", INT32_MAX, &args, 5), 0);
    amxc_var_dump(&args, STDOUT_FILENO);
    amxc_var_clean(&args);

    retval = test_rlyeh_status("6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.18");
    assert_int_equal(retval, amxd_status_ok);

    images = amxd_dm_findf(dm, RLYEH_DM_IMAGES);
    assert_non_null(images);
    images_count = amxd_object_get_instance_count(images);
    assert_int_equal(images_count, 3);

    amxd_object_t* image_alpine_annotated_318 = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", "6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.18");
    assert_non_null(image_alpine_annotated_318);

    amxc_var_init(&args);

    amxd_object_get_params(image_alpine_annotated_318, &args, amxd_dm_access_protected);
    assert_true(GET_BOOL(&args, RLYEH_DM_IMAGE_MARK_RM));
    assert_non_null(GET_ARG(&args, RLYEH_DM_IMAGE_DISKLOCATION));

    amxc_var_clean(&args);

    amxd_object_t* image_alpine_annotated_319 = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", "6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.19");
    assert_non_null(image_alpine_annotated_319);

    amxc_var_init(&args);

    amxd_object_get_params(image_alpine_annotated_319, &args, amxd_dm_access_protected);
    assert_false(GET_BOOL(&args, RLYEH_DM_IMAGE_MARK_RM));
    assert_non_null(GET_ARG(&args, RLYEH_DM_IMAGE_DISKLOCATION));

    amxc_var_clean(&args);

    retval = test_rlyeh_gc();
    assert_int_equal(retval, amxd_status_ok);
    sleep(10);
    handle_events();

    amxc_var_init(&args);
    assert_int_equal(amxb_get(amxut_bus_ctx(), RLYEH_DM_IMAGES ".", INT32_MAX, &args, 5), 0);
    amxc_var_dump(&args, STDOUT_FILENO);
    amxc_var_clean(&args);

    images = amxd_dm_findf(dm, RLYEH_DM_IMAGES);
    assert_non_null(images);
    images_count = amxd_object_get_instance_count(images);
    assert_int_equal(images_count, 2);

    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated/index.json"));
    assert_true(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated/oci-layout"));

    SAH_TRACEZ_INFO(__func__, "Remove 6f3b69f0-482d-5eab-9df8-b9e856f6cb38:3.19");
    retval = test_rlyeh_remove("6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.19");
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(10);
    handle_events();

    amxc_var_init(&args);
    assert_int_equal(amxb_get(amxut_bus_ctx(), RLYEH_DM_IMAGES ".", INT32_MAX, &args, 5), 0);
    amxc_var_dump(&args, STDOUT_FILENO);
    amxc_var_clean(&args);

    retval = test_rlyeh_status("6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.19");
    assert_int_equal(retval, amxd_status_ok);

    image_alpine_annotated_319 = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", "6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.19");
    assert_non_null(image_alpine_annotated_319);

    amxc_var_init(&args);

    amxd_object_get_params(image_alpine_annotated_319, &args, amxd_dm_access_protected);
    assert_true(GET_BOOL(&args, RLYEH_DM_IMAGE_MARK_RM));
    assert_non_null(GET_ARG(&args, RLYEH_DM_IMAGE_DISKLOCATION));

    amxc_var_clean(&args);

    test_rlyeh_gc();
    handle_events();
    sleep(10);
    handle_events();

    amxc_var_init(&args);
    assert_int_equal(amxb_get(amxut_bus_ctx(), RLYEH_DM_IMAGES ".", INT32_MAX, &args, 5), 0);
    amxc_var_dump(&args, STDOUT_FILENO);
    amxc_var_clean(&args);

    images = amxd_dm_findf(dm, RLYEH_DM_IMAGES);
    assert_non_null(images);
    images_count = amxd_object_get_instance_count(images);
    assert_int_equal(images_count, 1);

    assert_false(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated/index.json"));
    assert_false(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine_annotated/oci-layout"));

    SAH_TRACEZ_INFO(__func__, "Remove dad7ed90-ee42-5e86-a229-618ba170d4a4:latest");
    retval = test_rlyeh_remove("dad7ed90-ee42-5e86-a229-618ba170d4a4", "latest");
    assert_int_equal(retval, amxd_status_ok);
    handle_events();
    sleep(10);
    handle_events();

    retval = test_rlyeh_status("dad7ed90-ee42-5e86-a229-618ba170d4a4", "latest");
    assert_int_equal(retval, amxd_status_ok);

    amxc_var_init(&args);
    assert_int_equal(amxb_get(amxut_bus_ctx(), RLYEH_DM_IMAGES ".", INT32_MAX, &args, 5), 0);
    amxc_var_dump(&args, STDOUT_FILENO);
    amxc_var_clean(&args);

    amxd_object_t* image_alpine_latest = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", "dad7ed90-ee42-5e86-a229-618ba170d4a4", "latest");
    assert_non_null(image_alpine_latest);

    amxc_var_init(&args);

    amxd_object_get_params(image_alpine_latest, &args, amxd_dm_access_protected);
    assert_true(GET_BOOL(&args, RLYEH_DM_IMAGE_MARK_RM));
    assert_non_null(GET_ARG(&args, RLYEH_DM_IMAGE_DISKLOCATION));

    amxc_var_clean(&args);

    test_rlyeh_gc();
    sleep(10);
    handle_events();

    amxc_var_init(&args);
    assert_int_equal(amxb_get(amxut_bus_ctx(), RLYEH_DM_IMAGES ".", INT32_MAX, &args, 5), 0);
    amxc_var_dump(&args, STDOUT_FILENO);
    amxc_var_clean(&args);

    images = amxd_dm_findf(dm, RLYEH_DM_IMAGES);
    assert_non_null(images);
    images_count = amxd_object_get_instance_count(images);
    assert_int_equal(images_count, 0);

    assert_false(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/index.json"));
    assert_false(file_exists("/tmp/rlyeh_test_datamodel/images/lcmtestcontainers/alpine/oci-layout"));

    retval = test_rlyeh_status("dad7ed90-ee42-5e86-a229-618ba170d4a4", "latest");
    assert_int_equal(retval, amxd_status_object_not_found);

    retval = test_rlyeh_status("6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.19");
    assert_int_equal(retval, amxd_status_object_not_found);

    retval = test_rlyeh_status("6f3b69f0-482d-5eab-9df8-b9e856f6cb38", "3.18");
    assert_int_equal(retval, amxd_status_object_not_found);

    handle_events();
    test_rlyeh_list();
    handle_events();

    test_rlyeh_gc();
    handle_events();
    sleep(10);
    handle_events();

    assert_true(rlyeh_is_empty_dir("/tmp/rlyeh_test_datamodel/blobs/sha256"));

    SAH_TRACEZ_INFO(__func__, "Stop rlyeh");
    _rlyeh_main(1, dm, parser);
}

void test_dm_sv(UNUSED void** state) {
    int retval;
    amxc_var_t args;
    amxd_object_t* rlyeh_obj = NULL;

    sahTraceAddZone(500, __func__);
    SAH_TRACEZ_INFO(__func__, "Start rlyeh");
    _rlyeh_main(0, dm, parser);
    handle_events();

    retval = test_rlyeh_sv(false);
    assert_int_equal(retval, amxd_status_ok);

    rlyeh_obj = amxd_dm_get_object(dm, RLYEH_DM);
    assert_non_null(rlyeh_obj);
    amxc_var_init(&args);
    amxd_object_get_params(rlyeh_obj, &args, amxd_dm_access_protected);
    assert_false(GET_BOOL(&args, RLYEH_SIGNATURE_VERIFICATION));
    amxc_var_clean(&args);

    retval = test_rlyeh_sv(true);
    assert_int_equal(retval, amxd_status_ok);

    rlyeh_obj = amxd_dm_get_object(dm, RLYEH_DM);
    assert_non_null(rlyeh_obj);
    amxc_var_init(&args);
    amxd_object_get_params(rlyeh_obj, &args, amxd_dm_access_protected);
    assert_true(GET_BOOL(&args, RLYEH_SIGNATURE_VERIFICATION));
    amxc_var_clean(&args);

    SAH_TRACEZ_INFO(__func__, "Stop rlyeh");
    _rlyeh_main(1, dm, parser);
}
