MACHINE = $(shell $(CC) -dumpmachine)
SHELL = /bin/bash

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv)

HEADERS = $(wildcard $(INCDIR)/$(TARGET)/*.h)
SOURCES = $(wildcard $(SRCDIR)/rlyeh*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes \
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) \
		  -fprofile-arcs -ftest-coverage \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500 \
		  $(shell pkg-config --cflags cmocka)
LDFLAGS += -fprofile-arcs -ftest-coverage \
           -fkeep-inline-functions -fkeep-static-functions \
		$(shell pkg-config --libs cmocka) -lamxc \
		-lamxp -lamxd -lamxo -lamxb -lrlyeh -lamxut \
		-lsahtrace -lpthread -llcm
