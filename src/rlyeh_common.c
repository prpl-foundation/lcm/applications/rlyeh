/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/statfs.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <inttypes.h>
#include <sys/types.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/sendfile.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <rlyeh/rlyeh_utils.h>

#include <lcm/lcm_assert.h>

#include "rlyeh.h"

#define ME "rlyeh_common"

typedef ssize_t (* rec_dir_function_t)(const char*, const char*);

int64_t get_free_disk_space(const char* path) {
    struct statfs buffer;
    int64_t total_free_bytes;

    if(STRING_EMPTY(path)) {
        SAH_TRACEZ_ERROR(ME, "Path is NULL");
        return -1;
    }

    if(statfs(path, &buffer)) {
        SAH_TRACEZ_WARNING(ME, "Couldn't statfs '%s': (%d) %s", path, errno, strerror(errno));
        return -2;
    }

    total_free_bytes = buffer.f_bavail * buffer.f_bsize;

    SAH_TRACEZ_WARNING(ME, "Remaining size on '%s': %" PRId64, path, total_free_bytes);

    return total_free_bytes;
}

int rlyeh_mkdir(const char* dir, bool recursive) {
    int res = -1;
    char* dir_cpy = NULL;
    char* p = NULL;
    if(STRING_EMPTY(dir)) {
        return res;
    }

    if(recursive) {
        dir_cpy = strdup(dir);

        p = dir_cpy;
        while(*p && *p == '/') {
            p++;
        }

        while(*p) {
            while(*p && *p != '/') {
                p++;
            }
            if(*p != '/') {
                break;
            }
            *p = '\0';
            if(mkdir(dir_cpy, 0755) != 0) {
                if(errno != EEXIST) {
                    SAH_TRACEZ_ERROR(ME, "Cannot create dir '%s' (%d): %s", dir_cpy, errno, strerror(errno));
                    goto exit;
                }
            }
            *p = '/';
            while(*p && *p == '/') {
                p++;
            }
        }
    }
    if(mkdir(dir, 0755) != 0) {
        if(errno != EEXIST) {
            SAH_TRACEZ_ERROR(ME, "Cannot create dir '%s' (%d): %s", dir, errno, strerror(errno));
            goto exit;
        }
    }
    res = 0;
exit:
    free(dir_cpy);
    return res;
}

static ssize_t rlyeh_symlink_file(const char* src, const char* dest) {
    if(symlink(src, dest)) {
        SAH_TRACEZ_ERROR(ME, "Cannot symlink '%s' to '%s' (%d): %s", src, dest, errno, strerror(errno));
        if(errno == EEXIST) {
            SAH_TRACEZ_ERROR(ME, "File already exists");
        }
        return -1;
    } else {
        return 1;
    }
}

static int rlyeh_recursive_dir(const char* src_dir, const char* dest_dir, rec_dir_function_t f) {
    int ret = 0;
    int helpret = 0;
    DIR* dir_ptr;
    struct dirent* entry;
    amxc_string_t path;
    amxc_string_t recursive_dest;
    amxc_string_init(&path, 0);
    amxc_string_init(&recursive_dest, 0);

    if((dir_ptr = opendir(src_dir)) == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot open src_dir: '%s'", src_dir);
        ret = -1;
    } else {
        while((entry = readdir(dir_ptr)) != NULL) {
            if(entry->d_name[0] != '.') {
                amxc_string_setf(&path, "%s/%s", src_dir, entry->d_name);
                amxc_string_setf(&recursive_dest, "%s/%s", dest_dir, entry->d_name);
                if((entry->d_type == DT_DIR) || (entry->d_type == DT_LNK)) {
                    helpret = rlyeh_mkdir(recursive_dest.buffer, false);
                    if(helpret) {
                        ret = -2;
                    } else {
                        helpret = rlyeh_recursive_dir(path.buffer, recursive_dest.buffer, f);
                        if(helpret) {
                            ret = -3;
                        }
                    }
                } else if((entry->d_type == DT_REG)) {
                    if(f(path.buffer, recursive_dest.buffer) < 1) {
                        ret = -4;
                    }
                }
            }
        }
        closedir(dir_ptr);
    }

    amxc_string_clean(&path);
    amxc_string_clean(&recursive_dest);
    return ret;
}

int rlyeh_copy_dir_recursively(const char* src_dir, const char* dest_dir) {
    if(STRING_EMPTY(src_dir) || STRING_EMPTY(dest_dir)) {
        return -255;
    }
    return rlyeh_recursive_dir(src_dir, dest_dir, rlyeh_copy_file);
}

int rlyeh_symlink_files_in_dir_recursively(const char* src_dir, const char* dest_dir) {
    // Note that the directories are not symlinked and will be recreated
    if(STRING_EMPTY(src_dir) || STRING_EMPTY(dest_dir)) {
        return -255;
    }
    return rlyeh_recursive_dir(src_dir, dest_dir, rlyeh_symlink_file);
}

int rlyeh_create_file(const char* file) {
    int output;

    if(STRING_EMPTY(file)) {
        return -1;
    }

    output = creat(file, 0665);
    if(output < 0) {
        SAH_TRACEZ_ERROR(ME, "Cannot create file '%s' (%d): %s", file, errno, strerror(errno));
        return -2;
    } else {
        close(output);
        return 0;
    }
}
