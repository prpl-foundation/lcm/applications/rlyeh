/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <amxc/amxc_llist.h>
#include <amxc/amxc_variant.h>
#include <debug/sahtrace.h>

#include <rlyeh/rlyeh_defines.h>

#include <lcm/lcm_assert.h>

#include "rlyeh_worker.h"
#include "rlyeh_worker_func.h"
#include "rlyeh_dm_notif.h"
#include "rlyeh_common.h"

#define ME "rlyeh_functions"
typedef struct _rlyeh_worker {
    pthread_t thread;
    amxc_llist_t tasks;
    bool isRunning;
    bool waitForTaskToFinish;
} rlyeh_worker_t;

typedef struct _rlyeh_worker_task {
    amxd_object_t* obj;
    amxc_var_t data;
    bool wait_for_finised_sig;
    rlyeh_exec_func_t func;
    amxc_llist_it_t it;
} rlyeh_worker_task_t;

static rlyeh_worker_t worker;

static pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t mutex_tasks = PTHREAD_MUTEX_INITIALIZER;


static void rlywh_worker_task_delete(rlyeh_worker_task_t** task) {
    when_null(task, exit);
    when_null(*task, exit);

    amxc_var_clean(&(*task)->data);

    free(*task);
    *task = NULL;
exit:
    return;
}

static void rlyeh_worker_task_it_free(amxc_llist_it_t* t_it) {
    rlyeh_worker_task_t* task = amxc_llist_it_get_data(t_it, rlyeh_worker_task_t, it);
    rlywh_worker_task_delete(&task);
}

static int rlywh_worker_task_new(rlyeh_worker_task_t** task) {
    int retval = -1;
    when_null(task, exit);

    *task = (rlyeh_worker_task_t*) calloc(1, sizeof(rlyeh_worker_task_t));
    when_null(*task, exit);

    when_failed(amxc_var_init(&(*task)->data), error);
    when_failed(amxc_llist_it_init(&(*task)->it), error);
    retval = 0;
    goto exit;

error:
    rlywh_worker_task_delete(task);
exit:
    return retval;
}


void* rlyeh_worker_loop(UNUSED void* args) {
    int pthread_ret;
    SAH_TRACEZ_INFO(ME, "Start worker loop");
    do {
        // Wait a new task to be added in the list
        pthread_mutex_lock(&mutex_tasks);
        if(worker.isRunning &&
           (amxc_llist_is_empty(&worker.tasks) || worker.waitForTaskToFinish)) {
            do {
                pthread_ret = pthread_cond_wait(&cond, &mutex_tasks);
            } while(pthread_ret == ETIMEDOUT);
        }

        // if wakeup signal comes from cleanup, then exit
        if(!worker.isRunning) {
            pthread_mutex_unlock(&mutex_tasks);
            break;
        }
        amxc_llist_it_t* t_it = amxc_llist_take_first(&worker.tasks);
        pthread_mutex_unlock(&mutex_tasks);

        if(!t_it) {
            continue;
        }

        rlyeh_worker_task_t* task = amxc_llist_it_get_data(t_it, rlyeh_worker_task_t, it);
        if(task->wait_for_finised_sig) {
            pthread_mutex_lock(&mutex_tasks);
            worker.waitForTaskToFinish = true;
            pthread_mutex_unlock(&mutex_tasks);

        }
        tr181_fault_type_t ret = task->func(task->obj, &task->data);
        if(ret) {
            SAH_TRACEZ_ERROR(ME, "Task returned error  [%d]", ret);
        }
        rlywh_worker_task_delete(&task);

    } while(worker.isRunning);
    SAH_TRACEZ_INFO(ME, "End worker loop");
    return NULL;
}

/**
 * @brief
 *
 * @param obj
 * @param data data to pass to the function
 * @param func function to execut
 * @param wait_for_finished_sig if true, so not start the next task until the task notified it is finished.
 *  This should be done with @rlyeh_worker_signal_task_complete
 * @return int
 */
int rlyeh_worker_add_task(amxd_object_t* obj, amxc_var_t* data, void* func,
                          bool wait_for_finished_sig, bool add_to_front) {
    SAH_TRACEZ_INFO(ME, "Add task to worker thread list");
    int retval = -1;
    rlyeh_worker_task_t* task = NULL;

    when_failed(rlywh_worker_task_new(&task), error);

    if(data != NULL) {
        when_failed(amxc_var_move(&task->data, data), error);
    }
    task->obj = obj;
    task->func = func;
    task->wait_for_finised_sig = wait_for_finished_sig;
    pthread_mutex_lock(&mutex_tasks);
    if(add_to_front) {
        retval = amxc_llist_prepend(&worker.tasks, &task->it);
    } else {
        retval = amxc_llist_append(&worker.tasks, &task->it);
    }
    pthread_cond_signal(&cond);
    pthread_mutex_unlock(&mutex_tasks);
    when_failed(retval, error);

    goto exit;
error:
    rlywh_worker_task_delete(&task);
exit:
    return retval;
}

void rlyeh_worker_signal_task_complete(void) {
    pthread_mutex_lock(&mutex_tasks);
    SAH_TRACEZ_INFO(ME, "Task completed");
    worker.waitForTaskToFinish = false;
    pthread_cond_signal(&cond);
    pthread_mutex_unlock(&mutex_tasks);
}

int rlyeh_init_worker(void) {
    int res = -1;
    sigset_t fillset;
    sigset_t oset;
    worker.isRunning = true;
    worker.waitForTaskToFinish = false;
    if((res = amxc_llist_init(&worker.tasks)) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to initialize tasks llist [Error %d]", res);
        goto exit;
    }
    // mask all signals on the thread
    sigemptyset(&fillset);
    pthread_sigmask(SIG_SETMASK, &fillset, &oset);
    res = pthread_create(&worker.thread, NULL, rlyeh_worker_loop, NULL);
    pthread_sigmask(SIG_SETMASK, &oset, NULL);
    if(res != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to create worker thread [Error %d]", res);
        goto exit;
    }
    res = 0;
exit:
    return res;
}

void rlyeh_clean_worker(void) {
    SAH_TRACEZ_INFO(ME, "Clean worker Rlyeh");

    pthread_mutex_lock(&mutex_tasks);
    worker.isRunning = false;
    pthread_cond_signal(&cond);
    pthread_mutex_unlock(&mutex_tasks);

    pthread_join(worker.thread, NULL);

    pthread_cond_destroy(&cond);
    pthread_mutex_destroy(&mutex_tasks);
    amxc_llist_clean(&worker.tasks, rlyeh_worker_task_it_free);
}
