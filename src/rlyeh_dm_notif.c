/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <debug/sahtrace.h>

#include <rlyeh/rlyeh_defines.h>

#include <lcm/lcm_assert.h>

#include "rlyeh.h"
#include "rlyeh_dm_notif.h"

#define ME "rlyeh_notif"

static amxc_var_t* amxc_var_add_new_key_cstring_from_object(amxc_var_t* var,
                                                            const char* var_key,
                                                            amxd_object_t* obj,
                                                            const char* obj_key) {
    amxc_var_t* subvar = NULL;

    when_null(var, exit);
    subvar = amxc_var_add_new_key(var, var_key);
    when_null(subvar, exit);

    if(amxc_var_push(cstring_t, subvar, amxd_object_get_value(cstring_t, obj, obj_key, NULL)) != 0) {
        amxc_var_delete(&subvar);
    }

exit:
    return subvar;
}

static inline void add_objparam_to_htable(amxc_var_t* params, amxd_object_t* image, const char* key) {
    amxc_var_add_new_key_cstring_from_object(params, key, image, key);
}

static inline void fill_in_notification_image_data(amxc_var_t* params, amxd_object_t* image) {
    add_objparam_to_htable(params, image, RLYEH_DM_IMAGE_NAME);
    add_objparam_to_htable(params, image, RLYEH_DM_IMAGE_DISKLOCATION);
    add_objparam_to_htable(params, image, RLYEH_DM_IMAGE_DUID);
    add_objparam_to_htable(params, image, RLYEH_DM_IMAGE_URI);
    add_objparam_to_htable(params, image, RLYEH_DM_IMAGE_VERSION);
    add_objparam_to_htable(params, image, RLYEH_DM_IMAGE_VENDOR);
    add_objparam_to_htable(params, image, RLYEH_DM_IMAGE_DESCRIPTION);
    add_objparam_to_htable(params, image, RLYEH_DM_IMAGE_MARK_RM);
}

static amxc_var_t* rlyeh_notif_init_htable(amxd_object_t* obj,
                                           const char* command_id,
                                           amxc_var_t* notif) {
    amxc_var_t* params = NULL;

    amxc_var_set_type(notif, AMXC_VAR_ID_HTABLE);
    if(!STRING_EMPTY(command_id)) {
        amxc_var_add_key(cstring_t, notif, RLYEH_NOTIF_COMMAND_ID, command_id);
    }
    params = amxc_var_add_key(amxc_htable_t, notif, RLYEH_NOTIF_PARAMETERS, NULL);
    amxc_var_add_new_key_cstring_from_object(params, RLYEH_NOTIF_CONTAINER_ID, obj, "Name");

    return params;
}

static amxc_var_t* rlyeh_notif_init_llist(UNUSED amxd_object_t* obj,
                                          const char* command_id,
                                          amxc_var_t* notif) {
    amxc_var_t* list = NULL;

    amxc_var_set_type(notif, AMXC_VAR_ID_HTABLE);
    if(!STRING_EMPTY(command_id)) {
        amxc_var_add_key(cstring_t, notif, RLYEH_NOTIF_COMMAND_ID, command_id);
    }
    list = amxc_var_add_key(amxc_llist_t, notif, RLYEH_NOTIF_IMAGES, NULL);

    return list;
}

static void set_notif_error_type(amxc_var_t* notif, tr181_fault_type_t type,
                                 const char* command_id, const char* command,
                                 const char* reason, va_list args) {

    amxc_var_set_type(notif, AMXC_VAR_ID_HTABLE);
    if(!STRING_EMPTY(command_id)) {
        amxc_var_add_key(cstring_t, notif, RLYEH_NOTIF_COMMAND_ID, command_id);
    }
    amxc_var_add_key(uint64_t, notif, RLYEH_NOTIF_ERROR_TYPE, type);
    amxc_var_add_key(cstring_t, notif, RLYEH_NOTIF_ERROR_TYPESTR, tr181_fault_type_to_string(type));

    if(!STRING_EMPTY(command)) {
        amxc_var_add_key(cstring_t, notif, RLYEH_NOTIF_ERROR_COMMAND, command);
    }
    if(!STRING_EMPTY(reason)) {
        amxc_string_t r;
        amxc_string_init(&r, 0);
        amxc_string_vsetf(&r, reason, args);
        amxc_var_add_key(cstring_t, notif, RLYEH_NOTIF_ERROR_REASON, amxc_string_get(&r, 0));
        amxc_string_clean(&r);
    }
}


/*
   static void rlyeh_notif(amxd_object_t* obj, const char* command_id, const char* event) {
    amxc_var_t notif;
    amxc_var_init(&notif);

    if(obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "obj is null");
        goto exit;
    }
    rlyeh_notif_init_htable(obj, command_id, &notif);

    amxd_object_emit_signal(obj, event, &notif);

   exit:
    amxc_var_clean(&notif);
   }

 */

static inline void rlyeh_notif_image(amxd_object_t* image, const char* command_id, const char* event) {
    amxc_var_t notif;
    amxc_var_t* params = NULL;
    amxc_var_init(&notif);

    if(image == NULL) {
        SAH_TRACEZ_ERROR(ME, "obj is null");
        goto exit;
    }

    params = rlyeh_notif_init_htable(image, command_id, &notif);
    fill_in_notification_image_data(params, image);
    amxd_object_emit_signal(image, event, &notif);

exit:
    amxc_var_clean(&notif);
}

void rlyeh_notif_image_pulled(amxd_object_t* obj, const char* command_id) {
    rlyeh_notif_image(obj, command_id, RLYEH_NOTIF_IMAGE_PULLED);
}

void rlyeh_notif_image_remove_mark(amxd_object_t* obj, const char* command_id) {
    rlyeh_notif_image(obj, command_id, RLYEH_NOTIF_IMAGE_MARKED_RM);
}

void rlyeh_notif_image_removed(amxd_object_t* obj, const char* command_id) {
    rlyeh_notif_image(obj, command_id, RLYEH_NOTIF_IMAGE_REMOVED);
}

void rlyeh_notif_image_status(amxd_object_t* obj, const char* command_id) {
    rlyeh_notif_image(obj, command_id, RLYEH_NOTIF_IMAGE_STATUS);
}


void rlyeh_notif_error(amxd_object_t* obj, const char* command_id,
                       tr181_fault_type_t type, const char* command,
                       const char* reason, ...) {
    amxc_var_t notif;
    amxc_var_init(&notif);
    va_list args;

    if(!obj) {
        SAH_TRACEZ_ERROR(ME, "obj is null");
        goto exit;
    }

    va_start(args, reason);
    set_notif_error_type(&notif, type, command_id, command, reason, args);
    va_end(args);

    amxc_var_dump(&notif, STDOUT_FILENO);

    amxd_object_emit_signal(obj, RLYEH_NOTIF_ERROR, &notif);

exit:
    amxc_var_clean(&notif);
}

void rlyeh_notif_image_list(const char* command_id) {
    amxc_var_t notif;
    amxc_var_t* list = NULL;
    amxd_object_t* root = NULL;
    amxd_dm_t* dm = rlyeh_get_dm();
    amxd_object_t* images = amxd_dm_findf(dm, RLYEH_DM_IMAGES);
    amxc_var_init(&notif);

    list = rlyeh_notif_init_llist(NULL, command_id, &notif);

    amxd_object_for_each(instance, it, images) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        amxc_var_t params;
        amxc_var_init(&params);
        amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
        fill_in_notification_image_data(&params, instance);

        amxc_var_add(amxc_htable_t, list, amxc_var_constcast(amxc_htable_t, &params));

        amxc_var_clean(&params);
    }

    root = amxd_dm_get_object(dm, RLYEH_DM);
    amxd_object_emit_signal(root, RLYEH_NOTIF_IMAGE_LIST, &notif);

    amxc_var_clean(&notif);
}

const char* tr181_fault_type_to_string(tr181_fault_type_t err) {
    switch(err) {
    case tr181_fault_ok:
        return "No error";
    case tr181_fault_request_denied:
        return "Request Denied";
    case tr181_fault_invalid_arguments:
        return "Invalid Arguments";
    case  tr181_fault_unknown_exec_env:
        return "Unknown Execution Environment";
    case tr181_fault_du_to_ee_mismatch:
        return "Deployment Unit to Execution Environment Mismatch";
    case tr181_fault_duplicate_du:
        return "Duplicate Deployment Unit";
    case tr181_fault_system_resources_exceeded:
        return "System Resources Exceeded";
    case tr181_fault_invalid_du_state:
        return "Invalid Deployment Unit State";
    default:
        SAH_TRACEZ_WARNING(ME, "Unknown error [%d]", err);

    }
    return "Unknown error";
}
