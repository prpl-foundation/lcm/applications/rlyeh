include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean

install: all
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)
	$(foreach odl,$(wildcard ./odl/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/;)
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)$(LIBDIR)/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 config/default-policy.json $(DEST)/etc/amx/$(COMPONENT)/default-policy.json
	$(INSTALL) -d -m 0755 $(DEST)/usr/bin
	ln -sfr $(DEST)/usr/bin/amxrt $(DEST)/usr/bin/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/rlyeh.sh $(DEST)$(INITDIR)/$(COMPONENT)

package: all
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 ./odl/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)$(LIBDIR)/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 config/default-policy.json $(PKGDIR)/etc/amx/$(COMPONENT)/default-policy.json
	$(INSTALL) -d -m 0755 $(PKGDIR)/usr/bin
	rm -f $(PKGDIR)/usr/bin/$(COMPONENT)
	ln -sfr $(PKGDIR)/usr/bin/amxrt $(PKGDIR)/usr/bin/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/rlyeh.sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	# expand/substitute source wildcard instead of using destination directory: the destination directory can contain files from another artifact not intended for pcb_docgen use
	$(eval ODLFILES += $(wildcard ./odl/*.odl))

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test