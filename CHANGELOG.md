# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v2.5.0 - 2024-10-18(15:01:15 +0000)

### Other

- - [amx] Failing to restart processes with init scripts
- [Rlyeh] Refactor to have singular code paths for parsing the library of images

## Release v2.4.0 - 2024-07-15(13:37:58 +0000)

### Other

- [Rlyeh] Properly default blobs and images to /lcm
- LCM container signature verification

## Release v2.3.4 - 2024-05-15(08:33:56 +0000)

### Changes

- Make amxb timeouts configurable

## Release v2.3.3 - 2024-05-06(15:25:34 +0000)

### Other

- do not print password in the logs

## Release v2.3.2 - 2023-11-07(09:08:30 +0000)

### Fixes

- Compilation errors when using changed libamxo

## Release v2.3.1 - 2023-10-04(12:54:48 +0000)

### Other

- Opensource component

## Release v2.3.0 - 2023-09-28(12:40:14 +0000)

### Other

- make storage location configurable

## Release v2.2.0 - 2023-09-08(07:04:28 +0000)

### Other

- define capabilities needed by process

## Release v2.1.2 - 2023-09-07(10:48:29 +0000)

### Other

- Make component available on gitlab.softathome.com

## Release v2.1.1 - 2023-08-25(09:34:19 +0000)

### Other

- [Prpl] Init scripts do not have a shutdown target - LCM Team Components

## Release v2.1.0 - 2023-07-12(12:10:16 +0000)

### Other

- LCM containers onboarding solution - runtime

## Release v2.0.1 - 2023-06-23(11:45:37 +0000)

### Other

- Move signature from Annotations to installDU() parameter

## Release v2.0.0 - 2023-06-02(11:08:09 +0000)

### Other

- DU/EU Unique Identifier

## Release v1.0.4 - 2023-03-10(15:10:16 +0000)

## Release v1.0.3 - 2023-03-10(08:38:48 +0000)

### Other

- [Config] enable configurable coredump generation
- add option to only start new task when previous is marked as completed

## Release v1.0.2 - 2023-02-24(14:29:49 +0000)

### Other

- [Rlyeh] Add feature to disable certificate verification

## Release v1.0.1 - 2023-02-15(14:45:10 +0000)

### Fixes

- Replace LIBDIR to STAGINGLIBDIR in makefile

## Release v1.0.0 - 2023-02-14(13:46:48 +0000)

### Other

- move storage to /lcm

## Release v0.1.30 - 2023-01-30(15:55:29 +0000)

### Other

- update error codes with USP spec

## Release v0.1.29 - 2023-01-18(16:34:15 +0000)

### Other

- Add a remaining diskspace check to the pull

## Release v0.1.28 - 2023-01-10(14:49:54 +0000)

### New

- [import-dbg] Disable import-dbg by default for all amxrt plugin

## Release v0.1.27 - 2022-10-27(11:42:51 +0000)

### Fixes

- if an image already existed but was marked for removalwq, reset the MarkForRemoval value

## Release v0.1.26 - 2022-10-24(12:27:33 +0000)

### Other

- Cleanup error handling of Install DU

## Release v0.1.25 - 2022-10-07(14:44:11 +0000)

### Fixes

-  fix memory leaks

## Release v0.1.24 - 2022-10-06(16:56:55 +0000)

## Release v0.1.23 - 2022-10-05(20:12:34 +0000)

### Fixes

- change log level

## Release v0.1.22 - 2022-06-30(16:16:32 +0000)

### Other

- add logs to worker thread

## Release v0.1.21 - 2022-04-07(07:56:43 +0000)

### Other

- : Change bundle name algorithm due to BundleName too long

## Release v0.1.20 - 2022-04-05(11:40:08 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v0.1.19 - 2022-03-23(12:06:43 +0000)

### Other

- Error with notifications when installing 2 applications at the same time

## Release v0.1.18 - 2022-03-04(09:54:42 +0000)

## Release v0.1.17 - 2022-03-03(15:15:15 +0000)

### Fixes

-  Start rlyeh with LD_LIBRARY_PATH="/opt/prplos/usr/lib/"

## Release v0.1.16 - 2022-03-03(10:12:48 +0000)

### Other

- Incorrect Error handling in case of installing an already present DU

## Release v0.1.15 - 2022-01-10(18:30:56 +0000)

## Release v0.1.14 - 2022-01-07(14:38:04 +0000)

## Release v0.1.13 - 2021-12-23(10:57:50 +0000)

### Fixes

- fix NULL pointer access

## Release v0.1.12 - 2021-12-15(14:52:36 +0000)

## Release v0.1.11 - 2021-12-07(17:59:10 +0000)

## Release v0.1.10 - 2021-12-02(09:50:41 +0000)

### Fixes

- change init script name

## Release v0.1.9 - 2021-11-24(10:11:18 +0000)

### Other

- [Rlyeh] Add support for docker.io

## Release v0.1.8 - 2021-11-22(13:59:16 +0000)

## Release v0.1.7 - 2021-11-05(15:40:20 +0000)

### Other

- [Rlyeh] Expose the URL used to fetch the DU

## Release v0.1.6 - 2021-11-05(13:16:06 +0000)

### Other

- libRlyeh : keep track of shared layers

## Release v0.1.5 - 2021-11-04(16:54:37 +0000)

### Other

- [Rlyeh] Implement 'Status' parameter

## Release v0.1.4 - 2021-10-14(09:56:41 +0000)

## Release v0.1.3 - 2021-10-07(11:05:03 +0000)

### Other

- [Rlyeh] Fix rlyeh:image:removed notification

## Release v0.1.2 - 2021-09-22(14:30:38 +0000)

### Fixes

- Create dirs in init script

### Other

- : [Rlyeh] Verify checksum after pulling image's blobs

## Release v0.1.1 - 2021-09-14(12:39:23 +0000)

## Release v0.1.0 - 2021-09-13(09:22:13 +0000)

### New

- Add Description, Vendor and Version AND Name in the datamodel
- Issues : IOT-866 IOT-867 IOT-868 IOT-859 Add Description, Vendor and Version AND Name in the datamodel
- Skopeo to librlyeh

### Other

- and IOT-877

## Release v0.1.0 - 2021-09-03(09:48:30 +0000)

### New

- Initial release
